@extends("layouts.frame")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>AGEX</h1>
                <ol>
                    <li><a href="#gioi-thieu" >Giới thiệu</a></li>
                    <li>
                        <a href="#huong-dan" >Hướng dẫn</a>
                        <ol>
                            <li><a href="#trang-chu">Trang chủ</a></li>
                            <li><a href="#dang-nhap">Đăng nhập</a></li>
                            <li><a href="#trang-san-pham">Trang sản phẩm</a></li>
                            <li><a href="#trang-tai-khoan">Trang tài khoản</a></li>

                        </ol>
                    </li>
                    <li><a href="#khop-lenh" >Quá trình khớp lệnh</a></li>
                </ol>
                <div class="introduction">
                    <h1 id="gioi-thieu">Giới thiệu</h1>
                    <p>
                        AGEX là sàn giao dịch nông sản được tạo nên nhằm giúp kết nối những nhà cung cấp sản phẩm nông
                        nghiệp với những nhà phân phối sản phẩm.
                    </p>
                    <p>
                        Tham gia AGEX, bạn sẽ có cơ hội tìm ra và tiếp cận với đối tác của mình dễ hơn bao giờ hết.
                    </p>

                    <h1 id="huong-dan">Hướng dẫn</h1>
                    <h2 id="trang-chu">Trang chủ</h2>
                    <p>
                        Trang chủ của hệ thống là bảng các sản phẩm cùng với những nguyện vọng gần khớp với nhau nhất.
                        <img class="img-fluid"
                             alt="Giao diện đăng ký"
                             src="{{ \Illuminate\Support\Facades\Storage::url('introduction/home.png') }}">
                    </p>
                    <p>Chú thích:</p>
                    <ol>
                        <li>Các tùy chọn hiển thị danh sách.</li>
                        <li>Thời gian cập nhật mới nhất.</li>
                        <li>Nút cập nhật lại kết quả.</li>
                        <li>Giá trung bình trên mỗi đơn vị sản phẩm của ngày trước.</li>
                        <li>Giá của nguyện vọng có giá cao nhất của ngày trước.</li>

                        <li>Giá của nguyện vọng có giá thấp nhất của ngày trước.</li>
                        <li>Danh sách 3 nguyện vọng mua gần với giá khớp lệnh nhất của mỗi sản phẩm</li>
                        <li>Danh sách giao dịch được khớp lệnh mới nhất</li>
                        <li>Danh sách 3 nguyện vọng bán gần với giá khớp lệnh nhất của mỗi sản phẩm</li>
                        <li>Đánh dấu sản phẩm quan tâm</li>

                        <li>Danh sách sản phẩm</li>
                        <li>Giá trị của nguyện vọng mua gồm giá và số lượng có thể chia nhỏ.</li>
                        <li>Giá trị của giao dịch khớp lệnh mới nhất.</li>
                        <li>Giá trị của nguyện vọng bán gồm giá và số lượng không thể chia nhỏ.</li>
                    </ol>
                    <h2>Đăng ký</h2>
                    <p>
                        Để sử dụng hệ thống, bạn cần đăng ký tài khoản tại
                        <a href="{{ route('register') }}">{{ route('register') }}</a> hoặc vào trang <strong>Tài khoản</strong>
                        và chọn "Đăng ký ngay".
                    </p>
                    <p>
                        Bạn cần cung cấp thông tin đăng nhập và một số thông tin sử dụng để liên lạc gồm địa chỉ email và
                        số điện thoại để phục vụ cho việc liên lạc khi tìm được đối tác.
                    </p>
                    <p>
                        Sau khi đăng ký xong, hệ thống sẽ gửi email xác nhận, bạn cần vào email và xác nhận. Sau khi hoàn
                        thành xác nhận địa chỉ email, bạn đã có thể sử dụng tài khoản của mình.
                    </p>
                    <img class="img-fluid"
                         alt="Giao diện đăng ký"
                         src="{{ \Illuminate\Support\Facades\Storage::url('introduction/register.png') }}">

                    <h2 id="dang-nhap">Đăng nhập</h2>
                    <p>
                        Để đăng nhập, bạn vào trang <strong>Tài khoản</strong> và cung cấp thông tin đăng nhập.
                    </p>
                    <img class="img-fluid"
                         alt="Giao diện đăng nhập"
                         src="{{ \Illuminate\Support\Facades\Storage::url('introduction/login.png') }}">

                    <h2 id="trang-san-pham">Trang sản phẩm</h2>
                    <p>Để vào một trang sản phẩm, bạn hãy chọn vào tên của sản phẩm bạn muốn vào ở trang chủ</p>
                    <img class="img-fluid"
                         alt="Giao diện đăng nhập"
                         src="{{ \Illuminate\Support\Facades\Storage::url('introduction/product.png') }}">
                    <p>Chú thích:</p>
                    <ol>
                        <li>Bảng nguyện vọng mua.</li>
                        <li>Tên sản phẩm.</li>
                        <li>Bảng nguyện vọng bán.</li>
                        <li>Thanh trạng thái mức độ hoàn thành của nguyện vọng và vẫn đang chờ khớp lệnh.</li>
                        <li>Thống kê giá sản phẩm ngày qua.</li>

                        <li>Thanh trạng thái mức độ hoàn thành của nguyện vọng và hiện tại không thể tiếp tục khớp lệnh.</li>
                        <li>Mô tả của sản phẩm</li>
                        <li>Biểu đồ giá sản phẩm qua các ngày</li>
                        <li>Biểu mẫu đăng ký nguyện vọng mua</li>
                        <li>Khoản giá có thể yêu cầu nguyện vọng</li>

                        <li>Khối lượng sản phẩm mua phải chia hết cho bội số này</li>
                        <li>Biểu mẫu đăng ký nguyện vọng bán</li>
                    </ol>
                    <h2 id="trang-tai-khoan">Trang tài khoản</h2>
                    <p>
                        Vào trang tài khoản bạn có thể xem danh sách các nguyện vọng, giao dịch, thực hiện các chức năng trên
                        đó và có thể đăng xuất.
                    </p>
                    <img class="img-fluid"
                         alt="Giao diện đăng nhập"
                         src="{{ \Illuminate\Support\Facades\Storage::url('introduction/profile.png') }}">
                    <p>Chú thích</p>
                    <ol>
                        <li>Đăng xuất</li>
                        <li>Danh sách các nguyện vọng</li>
                        <li>Tạm ngừng khớp lệnh nguyện vọng</li>
                        <li>Kết thúc khớp lệnh nguyện vọng</li>
                        <li>Tiếp tục khớp lệnh nguyện vọng</li>

                        <li>Danh sách các giao dịch đã khớp lệnh</li>
                        <li>Xác định nguyện vọng hình thành nên giao dịch</li>
                        <li>Hủy giao dịch</li>
                        <li>Đánh giá giao dịch</li>
                        <li>Điểm đánh giá của giao dịch đã hoàn thành</li>
                    </ol>
                </div>

                <div class="rule">
                    <h1 id="khop-lenh">Quá trình khớp lệnh</h1>
                    <p>
                        Với mỗi nguyện vọng đặt ra, hệ thống sẽ thực hiện khớp lệnh theo nguyện vọng của bạn với giá cả
                        có lợi nhất cho bạn ngay tại thời điểm đăng ký nguyện vọng. Số dư sản phẩm còn lại sẽ được tiếp
                        tục khớp lệnh với giá mà bạn đã đưa ra.
                    </p>
                    <p>Ví dụ, bạn đang muốn đặt nguyện vọng sản phẩm và trạng thái sản phẩm như hình dưới</p>
                    <img class="img-fluid"
                         alt="Giao diện đăng nhập"
                         src="{{ \Illuminate\Support\Facades\Storage::url('introduction/example.png') }}">
                    <p>
                        Trường hợp nếu bạn đăng ký nguyện vọng bán với giá 27.000 với số lượng 500, thì nguyện vọng của bạn sẽ được
                        khớp lệnh như sau:
                    </p>
                    <ol>
                        <li>200kg tôm với giá 29.500</li>
                        <li>300kg tôm với giá 28.800</li>
                    </ol>
                    <p>
                        Trường hợp nếu bạn đăng ký nguyện vọng bán với giá 29.000 với số lượng 300, thì nguyện vọng của bạn sẽ được
                        khớp lệnh như sau:
                    </p>
                    <ol>
                        <li>200kg tôm với giá 29.500</li>
                    </ol>
                    <p>Số lượng còn lại là 100 kg tôm sẽ được đưa vào hàng chờ và sẽ được khớp lệnh với giá 29.000</p>
                    <p>
                        Sau khi khớp lệnh, một email thông báo khớp lệnh sẽ được gửi đến hộp thư điện tử của bạn với
                        thông tin của đối tác của bạn. Bạn và đối tác sẽ chủ động liên lạc và thực hiện giao dịch thực tế.
                        Sau khi thực hiện xong giao dịch, bạn hãy vui lòng quay lại hệ thống và đánh giá độ uy tín của
                        đối tác về giao dịch đã được thực hiện.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection