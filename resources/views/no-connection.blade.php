@extends('layouts.frame')
@section('content')
    <img class="logo-inactive" src="/storage/images/inactive.png" alt="inactive">
    <h1>Lỗi kết nối mạng.</h1>
@endsection
@section('addon')
    <style>
        .logo-inactive {
            width: 150px;
            margin-bottom: 10px;
        }
        body {
            margin-bottom: 0;
        }
        main {
            min-height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        h1 {
            margin: 0;
        }
        #app {
            margin: 0;
        }
        @media screen and (min-width: 768px) {
            main {
                min-height: 50vh;
            }
        }
    </style>
@endsection