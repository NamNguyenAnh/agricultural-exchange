<style>
    .container {
        max-width: 1000px;
        margin: auto;
    }
    .head, .main, .foot {
        width: 100%;
        padding: 20px;
    }
    .head, .foot {
        background-color: #268226;
        color: white;
        text-align: center;
    }
    .head img {
        display: block;
        width: 300px;
        margin: auto;
    }
    .main {
        background-color: white;
    }
    p.sincerely {
        margin-top: 40px;
    }
</style>
<div class="container">
    <div class="head">
        <img src="https://speakitout.net/storage/images/agex-logo.png" alt="AGEX Logo">
    </div>
    <div class="main">
        @yield('content')
        <p>Nếu có thắc mắc hay khiếu nại, bạn vui lòng liên hệ
            <a href="mailto:{{ env('MAIL_FROM_ADDRESS') }}">{{ env('MAIL_FROM_ADDRESS') }}</a>
        </p>
        <p class="sincerely">Trân trọng.</p>
        <p>AGEX</p>
    </div>
    <div class="foot">
        2018
    </div>
</div>