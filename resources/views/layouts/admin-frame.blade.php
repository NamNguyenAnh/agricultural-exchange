<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <link href="/css/admin.css" rel="stylesheet">
    <script src="{{ asset('js/admin-app.js') }}" ></script>
    <script src="{{ asset('js/admin.js') }}" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav>
        <div class="logo">
            <img src="/storage/images/agex-logo.png" alt="AGEX - Agricultural Exchange">
        </div>
        <ul id="main-menu" class="menu">
            <li>
                <a href="/admin">
                    <i class="menu-icon fas fa-dungeon"></i>
                    <span class="menu-text">Tổng quan</span>
                </a>
            </li>
            <li>
                <a href="/admin/products">
                    <i class="menu-icon fas fa-leaf"></i>
                    <span class="menu-text">Sản phẩm</span>
                </a>
            </li>
            <li>
                <a href="/admin/users">
                    <i class="menu-icon fas fa-users"></i>
                    <span class="menu-text">Người dùng</span>
                </a>
            </li>
            <li>
                <a href="/admin/role">
                    <i class="menu-icon fas fa-address-card"></i>
                    <span class="menu-text">Phân quyền</span>
                </a>
            </li>
            <li>
                <a href="/admin/setting">
                    <i class="menu-icon fas fa-cogs"></i>
                    <span class="menu-text">Thiết lập</span>
                </a>
            </li>
        </ul>
        <ul class="menu menu-bottom">
            <li>
                <a href="{{ route('logout') }}">
                    <i class="menu-icon fas fa-sign-out-alt"></i>
                    <span class="menu-text">Đăng xuất</span>
                </a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <div id="app">
        @yield('content')
    </div>
    <div id="notification-response">
        <div id="notification-message"></div>
    </div>
</main>
<script>
    const app = new Vue({
        el: '#app'
    });
</script>
</body>
</html>