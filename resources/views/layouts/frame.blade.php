<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="/manifest.json">

    <!-- CSRF Token -->

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="/vue-google-charts/vue-google-charts.browser.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}
    <link href="/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>
<body>
    <div id="loading">
        <img id="loading-icon" src="/storage/images/loading.gif" alt="Loading...">
    </div>
    <header>
        <div class="logo">
            <img src="/storage/images/agex-logo.png" alt="AGEX - Agricultural Exchange">
        </div>
        <nav>
            <ul id="main-menu" class="menu">
                <li>
                    <a href="/">
                        <i class="menu-icon fas fa-home"></i>
                        <span class="menu-text">Trang chủ</span>
                    </a>
                </li>
                <li>
                    <a href="/statistic">
                        <i class="menu-icon fas fa-chart-line"></i>
                        <span class="menu-text">Thống kê</span>
                    </a>
                </li>
                <li>
                    <a href="/introduce">
                        <i class="menu-icon fas fa-leaf"></i>
                        <span class="menu-text">Giới thiệu</span>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="menu-icon fas fa-user"></i>
                        <span class="menu-text">Tài khoản</span>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <main>
        <div id="app">
            @yield('content')
        </div>
        <div id="notification-response">
            <div id="notification-message"></div>
        </div>
    </main>
    @yield('addon')
    <footer></footer>
    <nav></nav>
    <script src="{{ asset('js/main.js') }}" async></script>
    <script>
        const app = new Vue({
            el: '#app',
            mounted: function () {
                window.loadingScreen(false);
            }
        });
    </script>
</body>
</html>