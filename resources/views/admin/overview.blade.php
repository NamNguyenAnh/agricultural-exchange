@extends('layouts/admin-frame')
@section('content')
    <h1 class="panel-title">Tổng quan</h1>
    <section id="numbering">
        <h2 class="section-title">Thống kê tuần</h2>
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 col-sm-4 col-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="section-title">Người dùng</h5>
                        </div>
                        <div class="card-body">
                            <div class="statistic">
                                <span class="old-value">{{$user_all - $user_new}} + </span>
                                <span class="new-value">{{$user_new}}</span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-6 col-sm-4 col-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="section-title">Nguyện vọng</h5>
                        </div>
                        <div class="card-body">
                            <div class="statistic">
                                <span class="old-value">{{$wish_all - $wish_new}} + </span>
                                <span class="new-value">{{$wish_new}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="section-title">Giao dịch</h5>
                        </div>
                        <div class="card-body">
                            <div class="statistic">
                                <span class="old-value">{{$trade_all - $trade_new}} + </span>
                                <span class="new-value">{{$trade_new}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="trade-log">
        <h2 class="section-title">Giao dịch</h2>
        <div id="trade-chart"></div>
        {!! $lava->render('LineChart', 'TradeHistory', 'trade-chart')  !!}
    </section>
@endsection
