@extends('layouts/admin-frame')
@section('content')
    <div class="user-manage">
        <h1 class="panel-title">Quản lý người dùng</h1>
        @include('admin.notification')
        <section id="user">
            <h2 class="section-title">Người dùng</h2>
            <div class="users">
                <div class="user-title">
                    <div class="user-info">Thông tin</div>
                    <div class="role">Chức vụ</div>
                    <div class="action">Khóa người dùng</div>
                </div>
                @foreach($users as $user)
                    <div class="user">
                        <div class="name">{{ $user->name }}</div>
                        <div class="role">{{ $user->role->name }}</div>
                        <div class="email">
                            <span>{{ explode('@', $user->email)[0] }}</span>@<span>{{ explode('@', $user->email)[1] }}</span>
                        </div>
                        <div class="action">
                            @if ($user->role->name != 'administrator' && $user->id != \Illuminate\Support\Facades\Auth::user()->id)
                                <form action="/admin/user/ban/{{ $user->id }}">
                                    <div class="form-group">
                                        <label for="reason">Lý do</label>
                                        <input type="text" name="reason">
                                    </div>
                                    <div class="form-group">
                                        <label for="duration">Thời gian (ngày)</label>
                                        <input type="number" name="duration">
                                        <p><small>0: không xác định thời gian gỡ khóa.</small></p>
                                    </div>
                                    <button><i class="fas fa-user-slash" ></i></button>
                                </form>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        @if ($banned->count() != 0)
        <section id="banned-user">
            <h2 class="section-title">Người dùng bị khóa</h2>
            <div class="users">
                <div class="user-title">
                    <div class="user-info">Thông tin</div>
                    <div class="role">Chức vụ</div>
                    <div class="action">Gỡ khóa người dùng</div>
                </div>
                @foreach($banned as $user)
                    <div class="user">
                        <div class="name">{{ $user->name }}</div>
                        <div class="role">{{ $user->role->name }}</div>
                        <div class="email">{{ $user->email }}</div>
                        <div class="action">
                            <a href="/admin/user/unban/{{ $user->id }}"><button><i class="fas fa-user-plus" ></i></button></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        @endif
    </div>
@endsection