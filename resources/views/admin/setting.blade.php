@extends('layouts/admin-frame')
@section('content')
    <h1>Thiết lập website</h1>
    <form method="post" action="{{ action('SettingController@save') }}">
        @csrf
        <fieldset>
            <legend>Nguyện vọng</legend>
            <div class="form-group">
                <label>Đơn vị tiền tệ:</label>
                <input type="text" name="currency" value="{{ $settings->currency }}" required>
            </div>
            <div class="form-group">
                <label>Biên độ giao động giá: (0-1)</label>
                <input type="number" name="priceAmplitude" value="{{ $settings->priceAmplitude }}" required>
            </div>
            <div class="form-group">
                <label>Bội số khối lượng giao dịch:</label>
                <input type="number" name="quantityMultiples" value="{{ $settings->quantityMultiples }}" min="1" required>
            </div>
            <div class="form-group">
                <label>Giới hạn số nguyện vọng:</label>
                <input type="number" name="dailyWishLimit" value="{{ $settings->dailyWishLimit }}" min="1" required>
                nguyện vọng/ngày
            </div>
        </fieldset>
        <button type="submit">Cập nhật</button>
    </form>
@endsection