<section id="message">
    @if (session('messages'))
        @foreach(session('messages') as $message)
            <message-notification message="{{ $message }}"></message-notification>
        @endforeach
    @endif
    @if (session('errors'))
        @if(is_array(session('errors')))
            @foreach(session('errors') as $error)
                <message-notification message="{{ $error }}" type="error"></message-notification>
            @endforeach
        @else
            @foreach(session('errors')->all() as $error)
                <message-notification message="{{ $error }}" type="error"></message-notification>
            @endforeach
        @endif
    @endif
</section>