@extends('layouts/admin-frame')
@section('content')
    <div class="product-manage">
        <h1 class="panel-title">Quản lý sản phẩm</h1>
        @include('admin.notification')
        <section id="trade-log">
            <h2 class="section-title">Thêm sản phẩm mới</h2>
            <form class="add-product-form" method="post" action="/admin/product/insert">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" required>Tên sản phẩm</label>
                    <input type="text" name="name" required>
                </div>
                <div class="form-group">
                    <label for="unit" required>Đơn vị</label>
                    <input type="text" name="unit" required>
                </div>
                <div class="form-group">
                    <label for="price" required>Giá tham khảo</label>
                    <input type="number" name="price" step="100" min="0" required>
                </div>
                <div class="form-group">
                    <label for="description" required>Mô tả</label>
                    <textarea name="description" cols="40" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <button>Thêm sản phẩm</button>
                </div>
            </form>
        </section>
        <section id="products">
            <h2 class="section-title">Danh sách sản phẩm</h2>
            <div class="product-list">
                <div class="product-info title">
                    <div class="product-name">Tên</div>
                    <div class="product-created">Ngày tạo</div>
                    <div class="product-updated">Ngày cập nhật</div>
                    <div class="product-action">Thao tác</div>
                </div>
                @foreach($products as $product)
                    <product-editor :id={{ $product->id }} name="{{ $product->name }}" created="{{ $product->created_at }}"
                                    updated="{{ $product->updated_at }}" description="{{ $product->description }}">
                    </product-editor>
                @endforeach
            </div>
        </section>
    </div>
@endsection