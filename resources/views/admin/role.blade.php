@extends('layouts.admin-frame')
@section('content')
    <h1>Quản lý phân quyền</h1>
    @include('admin.notification')
    <section id="add-role">
        <h2>Thêm chức vụ mới</h2>
        <form id="form-add-role" action="{{ action('RoleController@insertRole') }}" method="post">
            {{ @csrf_field() }}
            <div class="form-group">
                <label>Tên chức vụ:</label>
                <input type="text" name="name" autocomplete="off">
            </div>
            <div class="form-group">
                <label>Khả năng truy cập:</label>
                @foreach($capabilities as $name => $index)
                    <span class="capability-select extend-checkbox">
                        <input type="checkbox" name="capabilities[{{ $name }}]" value="{{ $index }}">
                        {{ str_replace('_', ' ', ucfirst(strtolower($name))) }}
                    </span>
                @endforeach
            </div>
            <div class="form-group">
                <button type="submit">Tạo chức vụ</button>
            </div>
        </form>
    </section>
    <section id="role-capabilities">
        <h2>Khả năng truy cập</h2>
        <div id="form-edit-role">
            <div class="form-heading">
                <div class="role-name">
                    Tên chức vụ
                </div>
                <div class="role-capabilities">
                    Khả năng truy cập
                </div>
                <div class="role-action">
                    Hành động
                </div>
                @foreach($capabilities as $capability => $index)
                    <div class="role-capability">
                        {{ str_replace('_', ' ', ucfirst(strtolower($capability))) }}
                    </div>
                @endforeach
            </div>
            @foreach($roles as $role)
                <form class="role" method="get" action="{{ action('RoleController@modifyRoleCapability', [ 'id' => $role->id]) }}">
                    <div class="role-name">
                        @if ($role->id == 1||$role->id == 2)
                            {{ $role->name }}
                        @else
                            <input type="text" name="name" value="{{ $role->name }}">
                        @endif
                    </div>
                    @foreach($capabilities as $capability)
                        <div class="role-capability extend-checkbox">
                            <input type="checkbox"
                                   name="capabilities[]"
                                   value="{{ $capability }}"
                                   {{ (in_array($capability, json_decode($role->capabilities))||$role->id == 2)?'checked':''  }}
                                   {{ ($role->id == 1||$role->id == 2)?'disabled':''  }}
                            >
                        </div>
                    @endforeach
                    <div class="role-update">
                        @if ($role->id != 1 && $role->id != 2)
                            <button type="submit">Cập nhật</button>
                            <a href="{{ action('RoleController@deleteRole', ['id' => $role->id]) }}">
                                <button type="button">Xóa</button>
                            </a>
                            <button type="reset">Cài đặt lại</button>
                        @endif
                    </div>
                </form>
            @endforeach
        </div>
    </section>
    <section id="user-role">
        <h2>Quản lý quản trị viên</h2>
        <div class="admin-manager">
            <div class="admin-list">
                <div class="form-heading">
                    <div class="admin-name">Tên</div>
                    <div class="admin-email">Email</div>
                    <div class="admin-role">Chức vụ</div>
                    <div class="admin-action">Hành động</div>
                </div>
                @foreach($admins as $admin)
                    <form method="get" action="{{ action('RoleController@grantUserRole') }}">
                        <div class="admin-name">
                            {{ $admin->name }}
                        </div>
                        <div class="admin-email">
                            {{ $admin->email }}
                            <input type="hidden" name="email" value="{{ $admin->email }}">
                        </div>
                        <div class="admin-role">
                            <select name="role">
                                @foreach($roles as $role)
                                    @if ($role->id == 1 ||
                                     ($role->id == 2 && \Illuminate\Support\Facades\Auth::user()->role_id !=2))
                                        @continue
                                    @endif
                                    <option value="{{ $role->id }}" {{ $role->id == $admin->role_id?"selected":"" }}>
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="admin-action">
                            <button type="submit">Cập nhật</button>
                            <a href="{{ action('RoleController@revokeUserRole', ['user_id' => $admin->id]) }}">
                                <button type="button">Thu hồi quyền</button>
                            </a>
                        </div>
                    </form>
                @endforeach
                <form method="get" action="{{ action('RoleController@grantUserRole') }}">
                    <div class="admin-name">
                        Thêm mới
                    </div>
                    <div class="admin-email">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="admin-role">
                        <select name="role" name="role">
                            @foreach($roles as $role)
                                @if ($role->id == 1)
                                    @continue
                                @endif
                                <option value="{{ $role->id }}">
                                    {{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="admin-action">
                        <button type="submit">Cấp quyền</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection