@php
/**
  * @var string $reason
  * @var int $duration
  * @var string $username
  */
@endphp
@extends('layouts.mail')
@section('content')
    <h1>Tài khoản của bạn đã bị khóa</h1>
    <p>Chào {{ $username }},</p>
    <p>Agex rất tiếc phải thông báo tài khoản của bạn đã bị khóa
        @if ($duration == 0)
            vĩnh viễn
        @else
            trong vòng {{ $duration }} ngày
        @endif
    </p>
    @if (!empty($reason))
        <p>vì lý do: <em>{{ $reason }}</em></p>
    @endif
    <p>Tất cả các nguyện vọng của bạn sẽ chuyển sang trạng thái tạm dừng và không thể tiếp tục khớp lệnh.</p>
    <p>Nếu có thắc mắc hay khiếu nại, bạn vui lòng liên hệ
        <a href="mailto:{{ env('MAIL_FROM_ADDRESS') }}">{{ env('MAIL_FROM_ADDRESS') }}</a>
    </p>
@endsection