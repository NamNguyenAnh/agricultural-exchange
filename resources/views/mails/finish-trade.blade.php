@php
    /**
      * @var string $heading
      * @var string $username
      * @var string $message
      */
@endphp
@extends('layouts.mail')
@section('content')
    <h1>{{ $heading }}</h1>
    <p>Chào {{ $username }}</p>
    <p>{{ $message }}</p>
@endsection
