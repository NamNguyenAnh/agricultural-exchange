@php
    /**
      * @var string $username
      * @var string $type
      * @var string $product_name
      * @var array $matched_list
      * @var int $remain
      * @var int total_matched
      * @var int total_wish
      * @var int total_cost
      */
@endphp
@extends('layouts.mail')
@section('content')
    <h1>Nguyện vọng đã được khớp lệnh</h1>
    <p>Chào {{ $username }},</p>
    <p>Nguyện vọng {{ $type.$product_name }} của bạn đã được khớp lệnh.</p>
    <p><strong>Số lượng:</strong> {{ $total_matched }} trong {{ $remain }} còn lại của tổng nguyện vọng {{ $total_wish }}</p>
    <p><strong>Thông tin đối tác:</strong></p>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        table td, table th {
            border: 2px solid black;
            padding: 10px;
            font-size: 14px;
        }
        .right {
            text-align: right;
        }
        .center {
            text-align: center;
        }
    </style>
    <table>
        <tr>
            <th>Tên đối tác</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Giá</th>
            <th>Số lượng</th>
            <th>Thành tiền</th>
        </tr>
        @foreach($matched_list as $matched)
            <tr>
                <td class="center">{{ $matched['user']->name }}</td>
                <td class="center">{{ $matched['user']->email }}</td>
                <td class="center">{{ $matched['user']->phone }}</td>
                <td class="right">{{ number_format($matched['price']) }}</td>
                <td class="right">{{ number_format($matched['matched']) }}</td>
                <td class="right">{{ number_format($matched['cost']) }}</td>
            </tr>
        @endforeach
        <tr>
            <td class="center" colspan="4"><strong>Tổng cộng</strong></td>
            <td class="right">{{ number_format($total_matched) }}</td>
            <td class="right">{{ number_format($total_cost) }}</td>
        </tr>
    </table>
@endsection