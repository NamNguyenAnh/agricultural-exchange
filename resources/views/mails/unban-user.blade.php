@php
/**
  * @var string $reason
  * @var int $duration
  * @var string $username
  */
@endphp
@extends('layouts.mail')
@section('content')
    <h1>Tài khoản của bạn đã được bỏ khóa</h1>
    <p>Chào {{ $username }},</p>
    <p>Tài khoản của bạn đã được bỏ khóa. Bạn có thể hoạt động lại trên hệ thống bình thường</p>
    <p>Bạn vui lòng đọc lại nguyên tắc và quy đinh sử dụng hệ thống để tránh bị khóa lần nữa.</p>
@endsection