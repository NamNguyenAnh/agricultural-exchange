@extends('layouts.frame')
@section('content')
    <div class="alert alert-primary" role="alert">
        Email xác nhận đã được gửi đến <strong>{{ session('email') }}</strong>. Vui lòng xác nhận để có thể đăng nhập hệ thống
    </div>
@endsection