<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Thông tin đăng nhập không đúng.',
    'verify' => 'Vui lòng xác thực email để có thể đăng nhập vào hệ thống',
    'throttle' => 'Đăng nhập sai quá nhiều lần. Vui lòng thử lại sau :seconds giây.',

];
