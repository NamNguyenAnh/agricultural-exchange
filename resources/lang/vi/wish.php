<?php
use \Illuminate\Support\Facades\Config;
return [
    'success' => [
        Config::get('wishstatus.POSTPONE') => 'Tạm hoãn khớp lệnh thành công',
        Config::get('wishstatus.PROCESSING') => 'Tiếp tục khớp lệnh thành công',
        Config::get('wishstatus.ABORT') => 'Nguyện vọng đã được hủy',
    ]
];