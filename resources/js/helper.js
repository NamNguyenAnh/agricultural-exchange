window.getCookiesAsJson = function() {
    let cookiesArray = (document.cookie).split("; ");
    let cookiesJson = {};
    cookiesArray.forEach((item) => {
        let key = item.split("=", 1)[0];
        cookiesJson[key] = item.substr(key.length + 1)
    });
    return cookiesJson;
};
window.popNotification = function(message) {
    document.getElementById("notification-message").innerText = message;
    jQuery("#notification-response").fadeIn(function(){
        setTimeout(function(){
            jQuery("#notification-response").fadeOut();
        }, 5000);
    });
};
window.deleteCookie = function(cookieName, path = "/") {
    document.cookie = `${cookieName}=; Path=${path}; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
};
window.translate = function(text) {
    let dictionary = {
        "buy": "mua",
        "sell": "bán",
        "queue": "chờ xử lý",
        "processing": "đang khớp lệnh",
        "postpone": "hoãn khớp lệnh",
        "abort": "hủy khớp lệnh",
        "matched": "đã khớp lệnh",
        "completed": "hoàn thành",
    };
    if (dictionary.hasOwnProperty(text)) {
        return dictionary[text];
    }
    return text;
};
window.loadingScreen = function (on = true) {
    if (on) {
        $("#loading").css("display", "");
    } else {
        $("#loading").css("display", "none");
    }
};
window.moneyUnit = "đồng";