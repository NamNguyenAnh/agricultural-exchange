
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./helper');

window.Vue = require('vue');
// Vue.use(VueGoogleCharts);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin({
    beforeMount: function () {
        window.loadingScreen();
    },
    mounted: function(){
        window.loadingScreen(false);
    },
    beforeUpdate: function () {
        window.loadingScreen();
    },
    updated: function(){
        window.loadingScreen(false);
    }
});
Vue.component('product-content', require('./components/ProductContent.vue').default);
Vue.component('wish-table', require('./components/WishTable.vue').default);
Vue.component('product-table', require('./components/ProductTable.vue').default);
Vue.component('wish-history', require('./components/WishHistory.vue').default);
Vue.component('rating-input', require('./components/RatingInput.vue').default);
Vue.component('login-form', require('./components/LoginForm.vue').default);
Vue.component('account', require('./components/Account.vue').default);
Vue.component('statistic', require('./components/Statistic.vue').default);

