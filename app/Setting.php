<?php
/**
 * Created by PhpStorm.
 * User: memphis
 * Date: 02-Dec-18
 * Time: 1:13 AM
 */

namespace App;

use Illuminate\Support\Facades\File;

/**
 * Class Setting
 * @package App
 *
 * @property float $priceAmplitude
 * @property string $currency
 * @property int $quantityMultiples
 * @property int $dailyWishLimit
 */
class Setting
{
    const PATH = __DIR__ .
    DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'storage' .
    DIRECTORY_SEPARATOR . 'app' .
    DIRECTORY_SEPARATOR .'settings.json';

    const SETTING_LIST = [
        'currency', 'priceAmplitude', 'introduction', 'quantityMultiples', 'dailyWishLimit',
    ];
    private $settings;

    public function __construct()
    {
        $this->settings = json_decode(File::get(self::PATH), true);
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (array_has($this->settings, $name)) {
            return $this->settings[$name];
        }
        throw new \Exception("Property '{$name}' not found.");
    }

    /**
     * @param $name
     * @param $value
     * @return void
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if (array_has($this->settings, $name)) {
            $this->settings[$name] = $value;
            return;
        }
        throw new \Exception("Property '{$name}' not found.");
    }

    public function save()
    {
        File::put(self::PATH, json_encode($this->settings));
    }
}