<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceLog extends Model
{
    //
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
