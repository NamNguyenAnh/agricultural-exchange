<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class TradeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $title = 'Nguyện vọng khớp lệnh';
    private $icon = '/storage/icons/logo-64.png';
    private $receiver;
    private $type;
    private $product_name;
    private $matched_list;
    private $remain;
    private $total_matched = 0;
    private $total_wish;
    private $total_cost = 0;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($receiver, $type, $product_name, $matched_list, $remain, $total)
    {
        $this->receiver = $receiver;
        $this->type = \Config::get('wishtype.BUY') == $type ? 'mua ' : 'bán ';
        $this->product_name = $product_name;
        $this->matched_list = $matched_list;
        $this->remain = $remain;
        $this->total_wish = $total;
        foreach ($matched_list as $matched) {
            $this->total_matched += $matched['matched'];
            $this->total_cost += $matched['cost'];
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            WebPushChannel::class,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->title)
            ->markdown('mails.match-wish', [
                'username' => $this->receiver->name,
                'type' => $this->type,
                'product_name' => $this->product_name,
                'matched_list' => $this->matched_list,
                'remain' => $this->remain,
                'total_matched' => $this->total_matched,
                'total_wish' => $this->total_wish,
                'total_cost' =>$this->total_cost,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title($this->title)
            ->body('Nguyện vọng '. $this->type . $this->product_name . ' đã khớp lệnh ' .
                $this->total_matched . '/' . $this->total_wish . '.')
            ->icon($this->icon);
    }
}
