<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $view;
    private $data;
    private $subject;

    /**
     * Create a new notification instance.
     *
     * @param string $view
     * @param array $mailData
     * @param string $subject
     */
    public function __construct(string $view = '', array $mailData = [], string $subject = 'null')
    {
        $this->view = $view;
        $this->data = $mailData;
        $this->subject = $subject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = new MailMessage;
        if ($this->subject != null) {
            $mail = $mail->subject($this->subject);
        }
        return $mail->markdown($this->view, $this->data);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
