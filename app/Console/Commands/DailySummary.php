<?php

namespace App\Console\Commands;

use App\PriceLog;
use App\Product;
use App\Setting;
use App\TradeLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DailySummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:daily {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Carbon $today
     */
    protected $today;

    /**
     * @var Carbon $today
     */
    protected $yesterday;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date');
        if (is_null($date)) {
            $this->today = Carbon::today();
        } else {
            $data = explode('-', $date);
            $this->today = Carbon::createFromDate($data[0], $data[1], $data[2]);
        }
		$this->yesterday = $this->today;
		$this->yesterday->subDay();

        try {
            DB::beginTransaction();
            $this->summaryPrice();
            $this->summaryRating();
            $this->resetWishCount();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    private function summaryPrice()
    {
        /**
         * @var Collection $summary
         */
        $table_trade = 'trade_logs';
        $table_wish = 'wish_items';
        $summary = DB::table($table_trade)->select(DB::raw(
            "$table_wish.product_id as product_id," .
            "sum($table_trade.quantity) as quantity," .
            "max($table_trade.price) as max," .
            "min($table_trade.price) as min," .
            "sum($table_trade.price * $table_trade.quantity)/sum($table_trade.quantity) as avg"
        ))
            ->join('wish_items', function($join) use ($table_trade, $table_wish){
                $join->on("$table_trade.buy_wish", '=', "$table_wish.id")
                    ->orOn("$table_trade.sell_wish", '=', "$table_wish.id");
            })
//            ->whereIn('status', [Config::get('wishstatus.MATCHED'), Config::get('wishstatus.COMPLETED')])
            ->whereBetween($table_trade . '.' . TradeLog::CREATED_AT, [$this->yesterday, $this->today])
            ->groupBy('product_id')
            ->orderBy('product_id')
            ->get();
        $summary = $summary->keyBy('product_id');
        $products = Product::select(['id', 'old_price'])->orderBy('id')->get();
        echo 'Log ' . $this->yesterday->toDateString();

        foreach ($products as $product) {
            /**
             * @var Product $product
             */
            $result = $summary->get($product->id);
            if (is_null($result)) {
                $result = (object)[
                    'max' => $product->old_price,
                    'min' => $product->old_price,
                    'avg' => $product->old_price,
                    'quantity' => 0,
                ];
            }
            Product::where('id', $product->id)->update([
                'deviation' => intval($result->avg) - $product->old_price,
                'old_price' => intval($result->avg),
                'max_price' => $result->max,
                'min_price' => $result->min
            ]);
            PriceLog::insert([
                'product_id' => $product->id,
                'date' => $this->yesterday->toDateString(),
                'price' => intval($result->avg),
                'quantity' => $result->quantity,
            ]);
        }
    }

    private function summaryRating()
    {
        $users = User::select(['id'])->whereNotNull('email_verified_at')->get();

        /**
         * @var Collection $trades
         */
        $table_trade = 'trade_logs';
        $table_wish = 'wish_items';
        $trades = TradeLog::select(DB::raw("$table_wish.user_id as user_id, $table_trade.seller_rating as rating"))
            ->join($table_wish, "$table_trade.sell_wish", '=', "$table_wish.id")
            ->unionAll (
                TradeLog::select(DB::raw("$table_wish.user_id as user_id, buyer_rating as rating"))
                    ->join($table_wish, "$table_trade.buy_wish", '=', "$table_wish.id")
            )
            ->whereBetween($table_trade.'.'.TradeLog::CREATED_AT, [$this->yesterday, $this->today])
            ->get();

        foreach ($users as $user) {
            $user_trade = $trades->filter(function($value) use ($user) {
                return $value->user_id == $user->id;
            });

            $rating_count = json_decode($user->rating_count, true);
            if (is_null($rating_count)) {
                $rating_count = [];
            }
            foreach ($user_trade as $trade) {
                if (!isset($rating_count[$trade->rating])) {
                    $rating_count[$trade->rating] = 0;
                }
                $rating_count[$trade->rating] += 1;
            }
            $total_star = 0;
            $total_trade = 0;
            foreach ($rating_count as $rating => $count) {
                $total_star += $rating * $count;
                $total_trade += $count;
            }
            try {
                $rating = $total_star / $total_trade;
            } catch (\Exception $exception) {
                $rating = null;
            }

            User::where('id', $user->id)->update([
                'rating' => $rating,
                'rating_count' => json_encode($rating_count)
            ]);
        }

    }

    private function resetWishCount() {
        $settings = new Setting();
        User::where('wish_remain', '!=', $settings->dailyWishLimit)->update([
            'wish_remain' => $settings->dailyWishLimit
        ]);
    }
}
