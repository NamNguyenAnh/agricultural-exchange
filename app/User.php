<?php

namespace App;

use App\Notifications\MailNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use NotificationChannels\WebPush\HasPushSubscriptions;
use \Illuminate\Auth\MustVerifyEmail as VerifyEmailTrait;

/**
 * Class User
 * @package App
 * @property string $name
 * @property array $role
 * @property float $rating
 * @property int $wish_remain
 * @property string $email_verified_at
 * @method notify(\Illuminate\Notifications\Notification $notification)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens,
        Notifiable,
        HasPushSubscriptions,
        SoftDeletes,
        VerifyEmailTrait
    ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
