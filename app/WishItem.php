<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * @property int $id
 * @property string $type
 * @property int $user_id
 * @property int $product_id
 * @property int $price
 * @property int $quantity
 * @property int $total
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \App\Product $product
 * @property \App\User $user
 * @property bool $allow_split
 *
 */
class WishItem extends Model
{
    /**
     * Disable update property
     */
    const UPDATED_AT = null;

    /**
     * Add product property this belong to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Add user property this belong to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function invertType()
    {
        switch ($this->type) {
            case Config::get('wishtype.BUY'):
                return Config::get('wishtype.SELL');
            case Config::get('wishtype.SELL'):
                return Config::get('wishtype.BUY');
            default:
                throw new \Exception('\'type\' property must be \'buy\' or \'sell\'');
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getPriceCompareOperator()
    {
        switch ($this->type) {
            case Config::get('wishtype.BUY'):
                return '<=';
            case Config::get('wishtype.SELL'):
                return '>=';
            default:
                throw new \Exception('\'type\' property must be \'buy\' or \'sell\'');
        }
    }
}
