<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int old_price
 * @property int deviation
 * @property int max_price
 * @property int min_price
 */
class Product extends Model
{
    use SoftDeletes;
}
