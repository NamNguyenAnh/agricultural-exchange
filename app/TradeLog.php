<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $price
 * @property int $quantity
 * @property string $status
 * @property int $buyer_rating
 * @property int $seller_rating
 * @property string $matched_at
 * @property string $updated_at
 * @property \App\Product $product
 * @property int $buy_wish
 * @property int $sell_wish
 * @property \App\WishItem $buyWish
 * @property \App\WishItem $sellWish
 * @property \App\User $buyer
 * @property \App\User $seller
 */
class TradeLog extends Model
{
    /**
     * Override updated_at column to matched_at
     *
     * @var string
     */
    const CREATED_AT  = 'matched_at';

    /**
     * Add buyer as user object
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function buyWish()
    {
        return $this->belongsTo(\App\WishItem::class, 'buy_wish');
    }

    /**
     * Add seller as user object
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sellWish()
    {
        return $this->belongsTo(\App\WishItem::class, 'sell_wish');
    }

    public function __get($key)
    {
        switch ($key) {
            case 'product':
                return is_null($this->buyWish) ? $this->sellWish->product : $this->buyWish->product;
            case 'buyer':
                return is_null($this->buyWish) ? null : $this->buyWish->user;
            case 'seller':
                return is_null($this->sellWish) ? null : $this->sellWish->user;
            default:
                return parent::__get($key);
        }
    }
}
