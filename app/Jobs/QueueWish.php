<?php

namespace App\Jobs;

use App\Http\Controllers\WishItemController;
use App\Notifications\MobileNotification;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;

class QueueWish implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * @var \App\User $user
     */
    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $user)
    {
        $this->data = $data;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = (new WishItemController())->handleAddWish($this->data, $this->user);
        $type = Config::get('wishtype.BUY') == $this->data['type'] ? 'mua' : 'bán';
        $product_name = Product::find($this->data['product_id'])->name;
        switch ($result['status']) {
            case Config::get('wishstatus.PROCESSING'):
                $this->user->notify(new MobileNotification(
                    'Nguyện vọng đã được đăng ký thành công',
                    "Nguyện vọng $type $product_name của bạn đã được đăng ký thành công."
                ));
                break;
            case Config::get('wishstatus.MATCHED'):
                 break;
            default:
                $this->user->notify(new MobileNotification(
                    'Đăng ký nguyện vọng thất bại',
                    "Nguyện vọng $type $product_name của bạn đã thất bại. Vui lòng thử lại sau."
                ));
        }
    }
}
