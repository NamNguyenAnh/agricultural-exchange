<?php

namespace App\Http\Controllers;

use App\Http\Resources\TradeLogCollection;
use App\Jobs\ScheduleUnban;
use App\TradeLog;
use App\User;
use App\WishItem;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class UserController extends Controller
{
    /**
     * Get user's wish and trade
     *
     * @param Request $request
     * @return array
     */
    public function getWishHistory(Request $request)
    {
        $user_id = $request->user()->id;
        $wishList = WishItem::with([
            'product' => function($query) { $query->withTrashed()->select(['id', 'name']); }
            ])
            ->where('user_id', $user_id)
            ->orderBy('created_at','desc')
            ->get();
        $tradeList = TradeLog::with(
            [
                'buyWish:id,user_id,product_id', 'buyWish.user:id,name,rating,email,phone',
                'buyWish' => function($query){ $query->with([
                    'product' => function($query) { $query->withTrashed()->select(['id', 'name']); }
                ]); },
                'sellWish:id,user_id', 'sellWish.user:id,name,rating,email,phone',
                'sellWish' => function($query){ $query->with([
                    'product' => function($query) { $query->withTrashed()->select(['id', 'name']); }
                ]); },
            ])
            ->join('wish_items', function ($join){
                $join->on('trade_logs.buy_wish', '=', 'wish_items.id')
                    ->orOn('trade_logs.sell_wish', '=', 'wish_items.id');
            })
            ->select(['trade_logs.*'])
            ->where('wish_items.user_id', $user_id)
//            ->orWhere('seller', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();

        return [
            'wish' => $wishList,
            'trade' => new TradeLogCollection($tradeList),
        ];
    }

    /**
     * Add user push endpoint
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request)
    {
        $data = $request->validate([
            'endpoint' => 'required',
            'key' => 'required',
            'auth' => 'required',
        ]);
        $request->user()->updatePushSubscription($data['endpoint'], $data['key'], $data['auth']);
        return response()->json(['message' => 'Đăng ký nhận thông báo thành công']);
    }

    /**
     * Delete push endpoint of user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unsubscribe(Request $request)
    {
        $data = $request->validate([
            'endpoint' => 'required',
        ]);
        $request->user()->deletePushSubscription($data['endpoint']);
        return response()->json(['message' => 'Hủy nhận thông báo thành công']);
    }

    /**
     * Load user management page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminPanel()
    {
        $users = User::with('role:id,name')->get();
        $banned = User::onlyTrashed()->get();
        return view('admin.users', [
            'users' => $users,
            'banned' => $banned,
        ]);
    }

    /**
     * Ban user and send announce mail to user.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function banUser(Request $request, int $id)
    {
        $data = $request->validate([
            'reason' => 'nullable|string',
            'duration' => 'required|integer|min:0'
        ]);
        try {
            /**
             * @var \App\User $user
             */
            $user = User::findOrFail($id);
            if ($user->id == Auth::user()->id) {
                return redirect('/admin/users')->with([
                    'errors' => [
                        'Không thể khóa chính bạn.'
                    ]
                ]);
            }
            if ($user->role->name == 'administrator') {
                return redirect('/admin/users')->with([
                    'errors' => [
                        'Không thể khóa người dùng là quản trị viên.'
                    ]
                ]);
            }

            $data['username'] = $user->name;
            $user->notify(new \App\Notifications\MailNotification(
                'mails.ban-user',
                $data,
                'Thông báo khóa tài khoản'));

            if ($token = $user->token()) {
                $token->revoke();
            }
            WishItem::where([
                ['user_id', '=', $id],
                ['status', '=', Config::get('wishstatus.PROCESSING')],
            ])->update([
                'status' => Config::get('wishstatus.SUSPEND')
            ]);
            $user->delete();

            ScheduleUnban::dispatch($id)->delay(now()->addDays($data['duration']));

            return redirect('/admin/users')->with([
                'messages' => [
                    'Người dùng đã bị khóa.'
                ]
            ]);
        } catch (ModelNotFoundException $exception) {
            return redirect('/admin/users')->with([
                'errors' => [
                    'Không tìm thấy người dùng.'
                ]
            ]);
        }
    }

    /**
     * Unban user and send announce mail to user.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function unbanUser(int $id)
    {
        try {
            /**
             * @var \App\User $user
             */
            $user = User::onlyTrashed()->findOrFail($id);
            $user->restore();
            WishItem::where([
                ['user_id', '=', $id],
                ['status', '=', Config::get('wishstatus.SUSPEND')],
            ])->update([
                'status' => Config::get('wishstatus.PROCESSING')
            ]);
            $user->notify(new \App\Notifications\MailNotification(
                'mails.unban-user',
                ['username' => $user->name],
                'Thông báo bỏ khóa tài khoản'));

            return redirect('/admin/users')->with([
                'messages' => [
                    'Người dùng đã được bỏ khóa.'
                ]
            ]);
        } catch (ModelNotFoundException $exception) {
            return redirect('/admin/users')->with([
                'errors' => [
                    'Không tìm thấy người dùng.'
                ]
            ]);
        }
    }
}

