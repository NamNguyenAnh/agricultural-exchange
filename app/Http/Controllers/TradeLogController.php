<?php

namespace App\Http\Controllers;

use App\Notifications\MailNotification;
use App\Notifications\TradeNotification;
use App\TradeLog;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\ValidationException;

class TradeLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $data = $request->validate([
            'status' => 'required|in:' . implode(',', Config::get('wishstatus')),
            'rating' => 'required_if:status,'.Config::get('wishstatus.COMPLETED').'|numeric|between:1,5'
        ]);
        try {
            $user = $request->user();
            /**
             * @var \App\TradeLog $trade_log
             */
            $trade_log = TradeLog::with(['buyerObject:id,name', 'sellerObject:id,name', 'product:id,name'])->findOrFail($id);
            $this->validateUpdate($trade_log, $request);

            /**
             * @var \App\User $partner
             */
            $partner = null;
            $type = '';
            $product = $trade_log->product->name;
            if ($user->id == $trade_log->buyer) {
                $partner = $trade_log->seller;
                $type = 'bán';
            } else {
                $partner = $trade_log->buyer;
                $type = 'mua';
            }

            if (isset($data['status']) && $data['status'] == Config::get('wishstatus.ABORT')) {
                $trade_log->status = $data['status'];
                $trade_log->save();
                $partner->notify(new MailNotification(
                    'finish-trade',
                    [
                        'heading' => 'Đối tác đã hủy giao dịch hủy.',
                        'username' => $user->name,
                        'message' => "Giao dịch {$type} {$product} với {$partner->name} đã bị đối tác hủy."
                    ],
                    'Đối tác đã hủy giao dịch hủy.'
                ));
                return response()->json(['message' => 'Giao dịch đã được hủy.']);
            } else if (isset($data['rating'])) {
                $trade_log->status = Config::get('wishstatus.COMPLETED');
                if ($user->id == $trade_log->buyer) {
                    $trade_log->seller_rating = $data['rating'];
                } else {
                    $trade_log->buyer_rating = $data['rating'];
                }
                $trade_log->save();

                if ($trade_log->seller_rating == null || $trade_log->buyer_rating == null) {
                    $partner->notify(new MailNotification(
                        'finish-trade',
                        [
                            'heading' => 'Giao dịch đã được đánh giá và hoàn tất.',
                            'username' => $user->name,
                            'message' => "Giao dịch $type $product với $partner->name đã được đối tác đánh giá và hoàn tất."
                        ],
                        'Giao dịch đã được đánh giá và hoàn tất.'
                    ));
                }

                return response()->json(['message' => 'Giao dịch đã được đánh giá hoàn tất.']);
            }

        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Không tìm thấy nguyện vọng'], 404);
        }
    }

    /**
     * @param TradeLog $trade_log
     * @param Request $request
     * @throws AuthenticationException
     * @throws \InvalidArgumentException
     */
    private function validateUpdate(TradeLog $trade_log, Request $request)
    {
        $user_id = $request->user()->id;
        if ($trade_log->buyer != $user_id && $trade_log->seller != $user_id ||
            $trade_log->buyer->id == $user_id && $trade_log->buyer_rating != null ||
            $trade_log->seller->id == $user_id && $trade_log->seller_rating != null) {
            throw ValidationException::withMessages([
                'Không có quyền cập nhật giao dịch này.'
            ]);
        }

        if ($trade_log->status == Config::get('wishstatus.ABORT')) {
            throw ValidationException::withMessages([
                'Trạng thái cập nhật không hợp lệ.'
            ]);
        }

    }
}
