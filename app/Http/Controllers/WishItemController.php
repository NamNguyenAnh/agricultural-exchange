<?php

namespace App\Http\Controllers;

use App\Http\Resources\WishItemCollection;
use App\Jobs\QueueWish;
use App\Notifications\TradeNotification;
use App\Product;
use App\Setting;
use App\TradeLog;
use App\User;
use App\WishItem;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

class WishItemController extends Controller
{
    const DEFAULT_LIMIT = 20;
    const MAX_LIMIT = 100;

    /**
     * Return collection of WishItem
     *
     * @param Request $request
     * @return WishItemCollection
     */
    public function index(Request $request)
    {
        $args = $request->validate([
            'product_id' => 'required|int',
            'limit' => 'nullable|int',
            'type' => 'required|in:' . implode(',', \Config::get('wishtype')),
        ]);
        // Check limit response
        if (isset($args['limit']) && !empty($args['limit'])) {
            if ($args['limit'] > self::MAX_LIMIT) {
                $args['limit'] = self::MAX_LIMIT;
            }
        } else {
            $args['limit'] = self::DEFAULT_LIMIT;
        }

        $query = WishItem::select(['price', 'quantity', 'created_at'])
            ->where('product_id', $args['product_id'])
            ->where('type', $args['type'])
            ->where('status', Config::get('wishstatus.PROCESSING'))
            ->limit($args['limit'])
            ->orderBy('created_at', 'desc');

        return new WishItemCollection($query->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $user = $request->user();

        $data = $request->validate([
            'status' => 'required|in:' . implode(',', Config::get('wishstatus')),
        ]);

        try {
            $wish = WishItem::with('product:id,name')->findOrFail($id);
            $this->validateUpdate($wish, $request);
            $wish->status = $data['status'];
            $wish->save();
            return response()->json([
                'message' => Lang::get('wish.success.'.$data['status'])
            ]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Không tìm thấy nguyện vọng'], 404);
        } catch (AuthenticationException $exception) {
            return response()->json(['message' => 'Không có quyền cập nhật nguyện vọng này'], 403);
        } catch (\InvalidArgumentException $exception) {
            return response()->json(['message' => 'Trạng thái cập nhật không hợp lệ'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, int $id)
    {
        try {
            $user_id = $request->user()->id;
            /**
             * @var \App\WishItem $wish
             */
            $wish = WishItem::findOrFail($id);
            if ($wish->user_id != $user_id) {
                return response()->json(['message' => 'Không có quyền cập nhật nguyện vọng này'], 403);
            }
            $wish->status = Config::get('wishstatus.COMPLETED');
            $wish->save();
            DB::commit();
            return response()->json(['message' => 'Nguyện vọng được đánh dấu là hoàn thành.']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Không tìm thấy nguyện vọng'], 404);
        }
    }

    /**
     * Handle new wish-adding request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewWishItem(Request $request)
    {
        $user = $request->user();
        if (is_null($user)) {
            return abort(401);
        }
        if ($user->wish_remain < 1) {
            return response()->json(['message' => 'Bạn đã đặt quá số nguyện vọng tối đa trong ngày']);
        }
        $data = $request->validate([
            'type' => 'required|in:' . implode(',', Config::get('wishtype')),
            'product_id' => 'required|exists:products,id',
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            'allow_split' => 'nullable|boolean',
        ]);
        $price = DB::table('products')->where('id', $data['product_id'])->value('old_price');
        $settings = new Setting();
        $amplitude = $settings->priceAmplitude;
        if (($data['price'] > floor((1 + $amplitude) * $price)) || ($data['price'] < floor((1 - $amplitude) * $price)) ) {
            return response()->json(['message' => 'Giá đưa ra vượt quá biên độ giao động cho phép.'], 400);
        }

        QueueWish::dispatch($data, $user);
        User::where('id', $user->id)->update([
            'wish_remain' => $user->wish_remain - 1
        ]);
        return response()->json(['message' => 'Nguyện vọng đã dược ghi nhận và đang chờ xử lý.']);
    }

    public function handleAddWish($data, $user) {
        $settings = new Setting();
        $data['quantity'] *= $settings->quantityMultiples;

        if (!isset($data['allow_split'])) {
            $data['allow_split'] = false;
        }


        $newWish = new WishItem();
        $newWish->type = $data['type'];
        $newWish->user_id = $user->id;
        $newWish->product_id = $data['product_id'];
        $newWish->price = $data['price'];
        $newWish->quantity = $data['quantity'];
        $newWish->total = $data['quantity'];
        $newWish->allow_split = $data['allow_split'];
        $newWish->status = Config::get('wishstatus.PROCESSING');
        $newWish->save();

        $wishList =  $this->getMatchWishes($newWish);
        $result = $this->matchWish($newWish, $wishList);

        return $result;
    }

    /**
     * Handle buy request
     *
     * @param WishItem $newWish
     * @param Collection $wish_list
     * @return mixed|string|array
     */
    private function matchWish(WishItem $newWish, Collection $wish_list)
    {
        DB::beginTransaction();

        try {
            // Case there's no match wish.
            if ($wish_list->count() == 0) {
                DB::commit();
                return [
                    'status' => Config::get('wishstatus.PROCESSING'),
                ];
            }

            // Case there're at least 1 wish
            $matched_list = [];
            foreach ($wish_list as $wish) {
                /**
                 * @var \App\WishItem $wish
                 */
                $user = $wish->user;
                if ($newWish->quantity - $wish->quantity >= 0) {
                    $this->storeTradeLog($newWish, $wish, $newWish->type, $wish->price, $wish->quantity);
                    $matched_list[] = [
                        'user' => $user,
                        'price' => $wish->price,
                        'matched' => $wish->quantity,
                        'remain' => $wish->quantity,
                        'total' => $wish->total,
                        'cost' => $wish->price * $wish->quantity,
                    ];

                    WishItem::where('id', $wish->id)->update([
                        'quantity' => 0,
                        'status' => Config::get('wishstatus.COMPLETED'),
                    ]);

                    $newWish->quantity -= $wish->quantity;
                    if ($newWish->quantity == 0) break;
                } else {
                    if ($wish->allow_split) {
                        $matched_list[] = [
                            'user' => $user,
                            'price' => $wish->price,
                            'matched' => $newWish->quantity,
                            'remain' => $wish->quantity,
                            'total' => $wish->total,
                            'cost' => $wish->price * $newWish->quantity,
                        ];

                        $wish->quantity -= $newWish->quantity;
                        $this->storeTradeLog($newWish, $wish, $newWish->type, $wish->price, $newWish->quantity);
                        $wish->save();
                        $newWish->quantity = 0;
                        break;
                    } else {
                        continue;
                    }
                }
            }

            $this->notifyToMatchedUsers(
                User::find($newWish->user_id),
                $matched_list,
                Product::find($newWish->product_id)->name,
                $newWish->type,
                $newWish->total
            );

            if ($newWish->quantity == 0 ) {
                $newWish->status = Config::get('wishstatus.COMPLETED');
            }
            $newWish->save();

            DB::commit();
            return [
                'status' => Config::get('wishstatus.MATCHED'),
                'completed' => $newWish->total - $newWish->quantity,
            ];
        } catch (\Exception $exception) {
            DB::rollBack();
            return [
                'status' => 'abort',
                'log' => $exception->getMessage(),
            ];
        }
    }

    /**
     * Find matched sell wish
     *
     * @param WishItem $newWish
     * @return \App\WishItem|Collection
     * @throws \Exception
     */
    private function getMatchWishes(WishItem $newWish)
    {
        $type = $newWish->invertType();
        $product_id = $newWish->product_id;
        $price = $newWish->price;
        $quantity = $newWish->quantity;
        $user_id = $newWish->user_id;

        if ($newWish->allow_split) {
            return WishItem::with(['product:id,name', 'user'])
                ->where([
                    ['product_id', '=', $product_id],
                    ['price', $newWish->getPriceCompareOperator(), $price],
                    ['quantity', '<=', $quantity],
                    ['allow_split', '=', false],
                    ['status', '=', Config::get('wishstatus.PROCESSING')],
                    ['type', '=',  $type],
                    ['user_id', '!=', $user_id],
                ])
                ->orWhere([
                    ['product_id', '=', $product_id],
                    ['price', $newWish->getPriceCompareOperator(), $price],
                    ['allow_split', '=', true],
                    ['status', '=', Config::get('wishstatus.PROCESSING')],
                    ['type', '=',  $type],
                    ['user_id', '!=', $user_id],
                ])
                ->orderBy('price', 'asc')
                ->orderBy('created_at', 'asc')
                ->get();
        } else {
            return WishItem::with('product:id,name')
                ->where([
                    ['product_id', '=', $product_id],
                    ['price', $newWish->getPriceCompareOperator(), $price],
                    ['quantity', '=', $quantity],
                    ['status', '=', Config::get('wishstatus.PROCESSING')],
                    ['type', '=',  $type],
                    ['user_id', '!=', $user_id],
                ])
                ->orWhere([
                    ['product_id', '=', $product_id],
                    ['price', $newWish->getPriceCompareOperator(), $price],
                    ['quantity', '>=', $quantity],
                    ['allow_split', '=', true],
                    ['status', '=', Config::get('wishstatus.PROCESSING')],
                    ['type', '=',  $type],
                    ['user_id', '!=', $user_id],
                ])
                ->orderBy('price', 'asc')
                ->orderBy('created_at', 'asc')
                ->get();
        }

    }

    /**
     * Save TradeLog to database
     *
     * @param \App\WishItem $matcher
     * @param \App\WishItem $matchee
     * @param string $action
     * @param int $price
     * @param int $quantity
     * @param string|null $status
     * @param null $matched_at
     */
    private function storeTradeLog(WishItem $matcher,WishItem $matchee, string $action, int $price, int $quantity)
    {
        $trade = new TradeLog();
        if ($action == 'buy') {
            $trade->buy_wish = $matcher->id;
            $trade->sell_wish = $matchee->id;
        } else {
            $trade->buy_wish = $matchee->id;
            $trade->sell_wish = $matcher->id;
        }

        $trade->price = $price;
        $trade->quantity = $quantity;
        $trade->status = Config::get('wishstatus.MATCHED');

        $trade->save();
    }

    /**
     * @param WishItem $wish
     * @param Request $request
     * @throws AuthenticationException
     * @throws \InvalidArgumentException
     */
    private function validateUpdate(WishItem $wish, Request $request)
    {
        $user_id = $request->user()->id;
        if ($user_id != $wish->user_id) {
            throw new AuthenticationException();
        }

        $action_list = [
            Config::get('wishstatus.ON_QUEUE') => [],
            Config::get('wishstatus.PROCESSING') => [
                Config::get('wishstatus.POSTPONE'),
                Config::get('wishstatus.ABORT'),
            ],
            Config::get('wishstatus.POSTPONE') => [
                Config::get('wishstatus.PROCESSING'),
                Config::get('wishstatus.ABORT'),
            ],
            Config::get('wishstatus.ABORT') => [],
            Config::get('wishstatus.MATCHED') => [
                Config::get('wishstatus.ABORT'),
                Config::get('wishstatus.COMPLETED'),
            ],
            Config::get('wishstatus.COMPLETED') => [],
        ];
        if(!in_array($request->status, $action_list[$wish->status])) {
            throw new \InvalidArgumentException();
        };
    }

    /**
     * @param User $host
     * @param array $matched_list
     * @param string $product_name
     * @param string $wish_type_of_host
     * @param int $total_wish
     */
    private function notifyToMatchedUsers(User $host, array $matched_list, string $product_name,
                                          string $wish_type_of_host, int $total_wish)
    {
        if (!in_array($wish_type_of_host, Config::get('wishtype'))) {
            throw new \InvalidArgumentException();
        }

        $wish_type_of_matchee = Config::get('wishtype.BUY') == $wish_type_of_host
            ? Config::get('wishtype.SELL')
            : Config::get('wishtype.BUY');

        foreach ($matched_list as $match) {
            $user = $match['user'];
            $user->notify(new TradeNotification(
                $user,
                $wish_type_of_matchee,
                $product_name,
                [[
                    'user' => $host,
                    'price' => $match['price'],
                    'matched' => $match['matched'],
                    'cost' => $match['price'] * $match['matched'],
                ]],
                $match['remain'],
                $match['total']
            ));
        }
        $host->notify(new TradeNotification($host, $wish_type_of_host, $product_name, $matched_list, $total_wish, $total_wish));
    }

}
