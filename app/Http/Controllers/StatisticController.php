<?php

namespace App\Http\Controllers;

use App\PriceLog;
use App\Product;
use App\TradeLog;
use App\WishItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    public function getProductList()
    {
        return response()->json(['products' => Product::select(['id', 'name'])->get()->toArray()]);
    }

    public function getStatistic(Request $request)
    {
        $data = $request->validate([
            'type' => 'required|string',
            'product' => 'required_if:type,price',
            'date-from' => 'required|date',
            'date-to' => 'required|date'
        ]);

        switch ($data['type']) {
            case 'price':
                return $this->getPriceStatistic($data);
            case 'wish':
                return $this->getWishStatistic($data);
            case 'trade':
                return $this->getTradeStatistic($data);
            default:
                return response()->json(['message' => 'Thông kê không tồn tại.']);
        }
    }

    private function getPriceStatistic($data)
    {
        $result = PriceLog::select(['date', 'price'])
            ->where('product_id', $data['product'])
            ->whereBetween('date', [$data['date-from'], $data['date-to']])
            ->get()->map(function($item){
                return array_values($item->toArray());
            });
        if ($result->count() > 0) {
            return response()->json(array_prepend($result->toArray(), ['Ngày', 'Giá']));
        }
        return response()->json([['Ngày', 'Giá'], [$data['date-from'], 0], [$data['date-to'], 0]]);
    }

    private function getWishStatistic($data)
    {
        $result = WishItem::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as wish_count'))
            ->whereBetween('created_at', [$data['date-from'], $data['date-to']])
            ->groupBy('date')
            ->get()->map(function($item){
                return array_values($item->toArray());
            });
        if ($result->count() > 0) {
            return response()->json(array_prepend($result->toArray(), ['Ngày', 'Số lượng']));
        }
        return response()->json([['Ngày', 'Số lượng'], [$data['date-from'], 0], [$data['date-to'], 0]]);
    }

    private function getTradeStatistic($data)
    {
        $result = TradeLog::select(DB::raw('DATE('+TradeLog::CREATED_AT+') as date'), DB::raw('count(*) as wish_count'))
            ->whereBetween('created_at', [$data['date-from'], $data['date-to']])
            ->groupBy('date')
            ->get()->map(function($item){
                return array_values($item->toArray());
            })->toArray();
        if ($result->count() > 0) {
            return response()->json(array_prepend($result->toArray(), ['Ngày', 'Số lượng']));
        }
        return response()->json([['Ngày', 'Số lượng'], [$data['date-from'], 0], [$data['date-to'], 0]]);
    }
}
