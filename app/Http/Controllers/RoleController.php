<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\ValidationException;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'admin:' . Config::get('capabilities.ROLE_MANAGER'));
    }

    public function index()
    {
        $capabilities = Config::get('capabilities');

        return view('admin.role', [
            'roles' => Role::all(),
            'capabilities' => $capabilities,
            'admins' => User::with('role')->where('role_id', '!=',1)->get(),
        ]);
    }

    public function insertRole(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|unique:roles,name',
            'capabilities' => 'required',
            'capabilities.*' => 'distinct|integer|in:' . $this->getCapabilityString(),
        ]);

        $data['capabilities'] = array_map(function($value){
            return intval($value);
        }, array_values($data['capabilities']));

        Role::insert([
            'name' => $data['name'],
            'capabilities' => json_encode($data['capabilities'])
        ]);

        return redirect()->action('RoleController@index')->with([
            'messages' => [
                'Thêm chức vụ mới thành công.'
            ]
        ]);
    }

    public function modifyRoleCapability(Request $request, int $id)
    {
        $data = $request->validate([
            'name' => 'required|unique:roles,name',
            'capabilities' => 'required',
            'capabilities.*' => 'distinct|in:' . $this->getCapabilityString(),
        ]);
        if ($id == 1 || $id == 2) {
            throw ValidationException::withMessages(['Role mặc định, không thể thay đổi']);
        }

        $data['capabilities'] = array_map(function($value){
            return intval($value);
        }, array_values($data['capabilities']));

        Role::where('id', $id)->update([
            'name' => $data['name'],
            'capabilities' => json_encode($data['capabilities']),
        ]);

        return redirect()->action('RoleController@index')->with([
            'messages' => [
                'Chức vụ đã được cập nhật.'
            ]
        ]);
    }


    public function deleteRole(Request $request, int $id)
    {
        if ($id == 1 || $id == 2) {
            throw ValidationException::withMessages(['Role mặc định, không thể thay đổi']);
        }

        User::where('role_id', $id)->update(['role_id' => 1]);
        Role::where('id', $id)->delete();

        return redirect('/admin/role')->with([
            'messages' => [
                'Chức vụ đã được xóa.'
            ]
        ]);
    }

    public function grantUserRole(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email|exists:users,email',
            'role' => 'required|int|exists:roles,id|not_in:1'
        ]);

        $user = User::where('email', $data['email'])->first();
        $role = Role::find($data['role']);

        if ($user->role_id != 2 && $data['role'] == 2) {
            throw ValidationException::withMessages([
                "Chỉ administrator có thể cấp vai trò administrator."
            ]);
        }

        if ($user->role_id == 2 && $data['role'] != 2 && User::where('role_id', 2)->count() == 1) {
            throw ValidationException::withMessages((["{$user->name} là quản trị viên duy nhất. Không thể cập nhật quyền khác."]));
        }

        $user->role_id = $data['role'];
        $user->save();

        return redirect(action('RoleController@index'))->with([
            'messages' => [
                "{$user->name} được đặt làm {$role->name}",
            ]
        ]);
    }

    public function revokeUserRole(Request $request, int $user_id)
    {

        try {
            $user = User::findOrFail($user_id);
            if (Auth::user()->role_id != 2 && $user->role_id == 2) {
                throw ValidationException::withMessages([
                    "Chỉ administrator có thể thu hồi vai trò administrator."
                ]);
            }
            $this->validateRevokeUser($request->user(), $user);
            $user->role_id = 1;
            $user->save();

            return redirect(action('RoleController@index'))->with([
                'messages' => [
                    "{$user->name} đã mất quyền quản trị viên.",
                ]
            ]);
        } catch (ModelNotFoundException $exception) {
            throw ValidationException::withMessages(['Không tìm thấy người dùng.']);
        }
    }

    private function getCapabilityString()
    {
        return implode(',', Config::get('capabilities'));
    }

    private function validateRevokeUser(User $actor, User $user)
    {
        if ($user->role_id == 1) {
            throw ValidationException::withMessages(["{$user->name} không phải là quản trị viên."]);
        }
        if ($user->role_id == 2 && User::where('role_id', 2)->count() == 1) {
            throw ValidationException::withMessages((["{$user->name} là quản trị viên duy nhất. Không thể thu hồi quyền."]));
        }
        if ($actor->id == $user->id) {
            throw ValidationException::withMessages(["Bạn không thể tự thu hồi quyền của mình"]);
        }
    }
}
