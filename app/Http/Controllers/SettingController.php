<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings = new Setting();
        return view('admin.setting', [
            'settings' => $settings
        ]);
    }

    public function save(Request $request)
    {
        $data = $request->validate([
            'currency' => 'required|string',
            'priceAmplitude' => ' required|numeric|between:0,1',
            'quantityMultiples' => ' required|integer|min:1',
        ]);

        $settings = new Setting();
        foreach ($data as $key => $value) {
            $settings->$key = $value;
        }
        $settings->save();
        return redirect()->action('SettingController@index')->with([
            'messages' => [
                'Cập nhật thành công.'
            ]
        ]);
    }
}
