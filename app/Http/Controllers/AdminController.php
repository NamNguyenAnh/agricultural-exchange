<?php

namespace App\Http\Controllers;

use App\TradeLog;
use App\User;
use App\WishItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{
    public function __invoke()
    {
        // Trade chart
        $from =  Carbon::today()->subDays(15);
        $created = TradeLog::CREATED_AT;
        $trades = TradeLog::select(DB::raw("count(*) as count, $created as date"))
            ->where($created, '>=', $from->toDateTimeString())
            ->orderBy('date')
            ->groupBy($created)
            ->get()->keyBy('date')->toArray();

        $created = WishItem::CREATED_AT;
        $wishes = WishItem::select(DB::raw("count(*) as count, $created as date"))
            ->where($created, '>', $from->toDateTimeString())
            ->orderBy('date')
            ->groupBy($created)
            ->get()->keyBy('date')->toArray();

        $lava = new Lavacharts();
        $trade_table = $lava->DataTable();
        $trade_table->addDateColumn('Ngày')
            ->addNumberColumn('Số lượng giao dịch')
            ->addNumberColumn('Số lượng nguyện vọng')
        ;
        for ($i = 0; $i <15; $i++) {
            $day = $from->addDays(1);
            $trade_table->addRow([
                $day->toDateString(),
                isset($trades[$day->toDateTimeString()]) ? $trades[$day->toDateTimeString()]['count'] : 0,
                isset($wishes[$day->toDateTimeString()]) ? $wishes[$day->toDateTimeString()]['count'] : 0,
            ]);
        }
        $lava->LineChart('TradeHistory', $trade_table);
        return view('admin.overview', [
            'lava' => $lava,
            'user_new' => User::where('created_at', '>=', Carbon::now()->startOfWeek())->count(),
            'user_all' => User::count(),
            'wish_new'=>WishItem::where($created, '>=', Carbon::now()->startOfWeek())->count(),
            'wish_all'=>WishItem::count(),
            'trade_new'=>TradeLog::where(TradeLog::CREATED_AT, '>=', Carbon::now()->startOfWeek())->count(),
            'trade_all'=>TradeLog::count(),
        ]);
    }
}
