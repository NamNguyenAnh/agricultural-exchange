<?php

namespace App\Http\Controllers;

use App\Http\Resources\PriceLogCollection;
use App\PriceLog;
use Illuminate\Http\Request;

class PriceLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $args = $request->validate([
            'product_id' => 'required|int',
            'limit' => 'int',
        ]);
        if (!isset($args['limit'])|| empty($args['limit']))
        {
            $args['limit'] = 30;
        }
        $data = PriceLog::select(['date', 'price'])->where('product_id', $args['product_id'])
            ->orderBy('date', 'desc')->limit($args['limit'])->get();
        return new PriceLogCollection($data);
    }
}
