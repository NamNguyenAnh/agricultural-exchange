<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use http\Exception\InvalidArgumentException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;

class TokenController extends Controller
{

    public function getPersonalToken(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'remember_me' => 'required|boolean',
        ]);
        $credential = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        if (!Auth::attempt($credential)) {
            throw new  AuthenticationException(Lang::get('auth.failed'));
        }
        /**
         * @var \App\User $user
         */
        $user = $request->user();
        if (is_null($user->email_verified_at)) {
            throw new  AuthenticationException(Lang::get('auth.verify'));
        }
        $tokenResult = $user->createToken('token_' . $user->id);
        $expires = 0;
        if ($data['remember_me']) {
            $time = Carbon::now()->addWeeks(1);
            $token = $tokenResult->token;
            $token->expires_at = $time;
            $token->save();
            $expires = $time;
        }

        return response()->json(['access_token' => $tokenResult->accessToken])->cookie($this->generateCookie([
            'name' => 'access_token',
            'value' => $tokenResult->accessToken,
            'httpOnly' => false,
            'expires' => $expires,
        ]));
    }

    public function revokeToken(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Logged out'
        ])->withCookie(Cookie::forget('access_token'));

    }

    /**
     * @param array $args
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    private function generateCookie(Array $args)
    {
        if (empty($args['name']) || empty($args['value'])) {
            throw new InvalidArgumentException('Argument must have name and value keys');
        }
        $defaults = [
            'path' => '/',
            'domain' => null,
            'expires' => 0,
            'secure' => false,
            'sameSite' => 'strict',
            'httpOnly' => true,
            'raw' => false,
        ];
        $fill_keys = array_keys(array_diff_key($defaults, $args));
        foreach ($fill_keys as $key) {
            $args[$key] = $defaults[$key];
        }
        return new \Symfony\Component\HttpFoundation\Cookie(
            $args['name'],
            $args['value'],
            $args['expires'],
            $args['path'],
            $args['domain'],
            $args['secure'],
            $args['httpOnly'],
            $args['raw'],
            $args['sameSite']
        );
    }
}
