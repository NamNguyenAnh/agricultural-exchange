<?php

namespace App\Http\Controllers;

use App\PriceLog;
use App\TradeLog;
use App\WishItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Product;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * API - Display a listing of the resource.
     *
     * @return ProductCollection
     */
    public function index()
    {
        return new ProductCollection(Product::all());
    }

    /**
     * API - Display the specified resource.
     *
     * @param  int  $id
     * @return ProductResource|JsonResponse
     */
    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            return new ProductResource($product);
        } catch (ModelNotFoundException $err) {
            return response()->json(['message' => 'Không tìm thấy sản phẩm'], 404);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadProductPage($id)
    {
        if (is_null(Product::find($id))) {
            return abort(404);
        }
        return view('product');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminPanel()
    {
        $products = Product::all();
        return view('admin.products', [
            'products' => $products
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insertProduct(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'unit' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer|min:0',
        ]);
        /**
         * @var \App\Product $product
         */
        $product = new Product();

        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->old_price = $data['price'];
        $product->deviation = 0;
        $product->max_price = $data['price'];
        $product->min_price = $data['price'];
        $product->save();

        return redirect('/admin/products')->with([
            'messages' => [
                'Thêm sản phẩm mới thành công.'
            ]
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteProduct(int $id)
    {
        try {
            /**
             * @var \App\Product $product
             */
            $product = Product::findOrFail($id);
            $product->delete();
            return redirect('/admin/products')->with([
                'messages' => [
                    'Xóa sản phẩm thành công.'
                ]
            ]);
        } catch (ModelNotFoundException $exception) {
            return redirect('/admin/products')->with([
                'errors' => [
                    'Không tìm thấy sản phẩm.'
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProduct(Request $request, int $id)
    {
        try {
            $data = $request->validate([
                'name' => 'required|string',
                'description' => 'required|string',
            ]);
            /**
             * @var \App\Product $product
             */
            $product = Product::findOrFail($id);

            $product->name = $data['name'];
            $product->description = $data['description'];
            $product->save();

            return redirect('/admin/products')->with([
                'messages' => [
                    'Cập nhật sản phẩm thành công.'
                ]
            ]);
        } catch (ModelNotFoundException $exception) {
            return redirect('/admin/products')->with([
                'errors' => [
                    'Không tìm thấy sản phẩm.'
                ]
            ]);
        }
    }

}
