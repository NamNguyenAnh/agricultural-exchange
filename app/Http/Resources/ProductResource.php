<?php

namespace App\Http\Resources;

use App\PriceLog;
use App\Setting;
use App\WishItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;
use Khill\Lavacharts\Lavacharts;

/**
 * @property mixed id
 * @property mixed name
 * @property mixed description
 * @property mixed old_price
 * @property mixed deviation
 * @property mixed min_price
 * @property mixed max_price
 */
class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $description = '<p>' .
            implode('</p><p>', explode(PHP_EOL, htmlentities($this->description))) .
            '</p>';
        $settings = new Setting();
        return [
            'product' => [
                'id' => $this->id,
                'name' => $this->name,
                'description' => $description,
                'price' => $this->old_price,
                'deviation' => $this->deviation,
                'max_price' => $this->max_price,
                'min_price' => $this->min_price,
                'unit' => $this->unit,
            ],
            'chart' => $this->generateChartData(),
            'wishes' => $this->getWishesByProduct(),
            'priceAmplitude' => $settings->priceAmplitude,
            'quantityMultiples' => $settings->quantityMultiples,
            'currency' => $settings->currency,
            'timestamps' => Carbon::now(),

        ];
    }

    private function generateChartData()
    {
        $data = [['Ngày', 'Giá']];
        $price_history = PriceLog::select(['date', 'price'])->where('product_id', $this->id)
            ->orderBy('date', 'desc')->limit(30)->get()->reverse();
        foreach ($price_history as $log) {
            $date = Carbon::createFromFormat('Y-m-d', $log->date);
            $data[] = ["{$date->day}/{$date->month}", $log->price];
        }
        return $data;
    }

    /**
     * @param int $product_id
     * @return array
     */
    private function getWishesByProduct()
    {
        $wishes = WishItem::select(['type', 'price', 'quantity', 'total', 'allow_split', 'status', 'created_at',])
            ->where('product_id', $this->id)
            ->limit(20)
            ->orderBy('created_at', 'desc')->get();

        return [
            'buy' => $this->filterWishesByType($wishes, Config::get('wishtype.BUY')),
            'sell' => $this->filterWishesByType($wishes, Config::get('wishtype.SELL')),
        ];
    }

    /**
     * @param Collection $wishes
     * @param $type
     * @return array
     */
    private function filterWishesByType(Collection $wishes, $type)
    {
        return $wishes->filter(function($wish) use ($type){
            return $wish->type == $type;
        })->toArray();
    }
}
