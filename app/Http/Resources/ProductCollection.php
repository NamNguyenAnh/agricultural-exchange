<?php

namespace App\Http\Resources;

use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    public $collects = '\App\Http\Resources\ProductCollectionItem';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'products' => $this->collection,
            'currency' => (new Setting())->currency,
            'timestamp' => Carbon::now()->toDateTimeString(),
        ];
    }
}
