<?php

namespace App\Http\Resources;

use App\PriceLog;
use App\TradeLog;
use App\WishItem;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Product;
use Illuminate\Support\Facades\Config;

class ProductCollectionItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'unit' => $this->unit,
            'reference' => $this->old_price,
            'highest' => $this->max_price,
            'lowest' => $this->min_price,
            'buy_wishlist' => new WishItemCollection(
                $this->getNearestWishesQuery(Config::get('wishtype.BUY'), false)->unionAll(
                    $this->getNearestWishesQuery(Config::get('wishtype.BUY'), true)
                )->orderBy('price', 'desc')->get()->reverse()
            ),
            'sell_wishlist' => new WishItemCollection(
                $this->getNearestWishesQuery(Config::get('wishtype.SELL'), false)->unionAll(
                    $this->getNearestWishesQuery(Config::get('wishtype.SELL'), true)
                )->orderBy('price', 'asc')->get()
            ),
            'matched' => new TradeLogResource(
                TradeLog::join('wish_items', 'trade_logs.buy_wish', '=', 'wish_items.id')
                    ->select(['trade_logs.price as price', 'trade_logs.quantity as quantity'])
                    ->where('wish_items.product_id', $this->id)
                    ->orderBy(TradeLog::CREATED_AT, 'desc')
                    ->first()
            ),
        ];
    }

    private function getNearestWishesQuery(string $type, bool $allow_split)
    {
        return WishItem::select(['allow_split', 'created_at', 'price', 'quantity'])->where([
            ['product_id', '=', $this->id],
            ['type', '=', $type],
            ['status', '=', Config::get('wishstatus.PROCESSING')],
            ['allow_split', '=', $allow_split],
        ])->limit(3);
    }
}
