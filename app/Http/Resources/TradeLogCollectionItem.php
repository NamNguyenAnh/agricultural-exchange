<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class TradeLogCollectionItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();
        $partner = $this->buyer->id == $user->id ? $this->seller: $this->buyer;

        return [
            'id' => $this->id,
            'type' => $this->buyer->id == $user->id ? Config::get('wishtype.BUY') : Config::get('wishtype.SELL'),
            'wish_id' => $this->buyer->id == $user->id ? $this->buy_wish : $this->sell_wish,
            'partner' => [
                'name' => $partner->name,
                'phone' => $partner->phone,
                'email' => $partner->email,
                'rating' => $partner->rating,
            ],
            'product' => $this->product,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'rating' => $this->buyer->id == $user->id ? $this->buyer_rating: $this->seller_rating,
            'matched_at' => $this->matched_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
