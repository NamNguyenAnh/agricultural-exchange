<?php

namespace App\Http\Middleware;

use Closure;

class AdminVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $capability
     * @return mixed
     */
    public function handle($request, Closure $next, $capability = 1)
    {
        if ($capability == 'admin' && $request->user()->role_id == 2) {
            return $next($request);
        }
        if ($request->user()->role_id == 2 || in_array($capability, json_decode($request->user()->role->capabilities))) {
            return $next($request);
        }
        return abort(403);
    }
}
