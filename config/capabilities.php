<?php
return [
    'ACCESS_PANEL' => 1,
    'PRODUCT_MANAGER' => 2,
    'USER_MANAGER' => 3,
    'ROLE_MANAGER' => 4,
    'SYSTEM_SETTING' => 5,
];