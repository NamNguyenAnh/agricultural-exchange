<?php
/**
 * Created by PhpStorm.
 * User: memphis
 * Date: 12-Oct-18
 * Time: 8:31 PM
 *
 * Statuses of wish item
 */
return [
    'ON_QUEUE' => 'queue',
    'PROCESSING' => 'processing',
    'POSTPONE' => 'postpone',
    'ABORT' => 'abort',
    'MATCHED' => 'matched',
    'COMPLETED' => 'completed',

    'SUSPEND' => 'suspend',
];