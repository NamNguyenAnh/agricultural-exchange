<?php
/**
 * Created by PhpStorm.
 * User: memphis
 * Date: 12-Oct-18
 * Time: 8:22 PM
 *
 * Type of wish item
 */
return [
    'BUY' => 'buy',
    'SELL' => 'sell',
];