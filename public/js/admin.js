jQuery(document).ready(function($){
    $(".extend-checkbox").click(function () {
        let checkBox = $(this).children("input[type=\"checkbox\"]");
        if (checkBox.length != 0) {
            checkBox.eq(0).click();
        }
    });
    $(".extend-checkbox input[type=\"checkbox\"]").click(function (e) {
        e.stopPropagation();
    });
});