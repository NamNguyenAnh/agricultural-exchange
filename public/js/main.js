const applicationServerPublicKey = "BD8rlgyBUwL5Lcvt4QzCHEe95Sxs7UQgFFbvhnm7664rO7ykwqJeNV9IZRbPcgDXBudzTGgHCuPlHxXDI5JWiW4=";
let isSubscribed = false;
let subscribeButton = null;
let swRegistration = null
if ('serviceWorker' in navigator && 'PushManager' in window) {
    navigator.serviceWorker
        .register('/sw.js')
        .then(function(swReg) {
            console.log('Service Worker Registered');
            swRegistration = swReg;
            initializeUI();
        });
}

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}
function initializeUI() {
    // Set the initial subscription value
    swRegistration.pushManager.getSubscription()
        .then(function(subscription) {
            isSubscribed = !(subscription === null);

            if (getCookiesAsJson().hasOwnProperty("access_token") && !isSubscribed) {
                $("body").prepend('<div class="subscribe-container">' +
                    '<div class="subscribe-text">Đăng ký nhận thông báo để nhận cập nhật sớm nhất</div>' +
                    '<button id="subscribe-button">Đăng ký</button>' +
                    '</div>');
                subscribeButton = document.getElementById("subscribe-button");
                subscribeButton.addEventListener('click', function() {
                    if (isSubscribed) {
                        unsubscribeUser();
                    } else {
                        subscribeUser();
                    }
                });
            } else if (!(getCookiesAsJson().hasOwnProperty("access_token")) && isSubscribed) {
                unsubscribeUser();
            }
        });
}

function subscribeUser() {
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
    })
    .then(function(subscription) {
        console.log('User is subscribed.');

        let subscribe = subscription.toJSON();
        axios.post(`${window.location.origin}/api/subscribe`, {
            endpoint: subscribe.endpoint,
            key: subscribe.keys.p256dh,
            auth: subscribe.keys.auth
        },{
            headers: {
                Authorization: `Bearer ${getCookiesAsJson().access_token}`
            }
        }).then(response => {
            $(".subscribe-container").slideUp(function () {
                $(this).remove();
            });
            window.popNotification(response.data.message);
        }).catch(err =>  {
            console.log(err.response);
        });

        isSubscribed = true;
    })
    .catch(function(err) {
        console.log('Failed to subscribe the user: ', err);
    });
}
function unsubscribeUser() {
    swRegistration.pushManager.getSubscription()
        .then(function(subscription) {
            if (subscription) {
                axios.post(`${window.location.origin}/api/unsubscribe`, {
                    endpoint: subscription.toJSON().endpoint,
                },{
                    headers: {
                        Authorization: `Bearer ${getCookiesAsJson().access_token}`
                    }
                }).then(response => {
                    window.popNotification(response.data.message);
                }).catch(err =>  {
                    console.log(err.response);
                });
                return subscription.unsubscribe();
            }
        })
        .catch(function(error) {
            console.log('Error unsubscribing', error);
        })
        .then(function() {
            console.log('User is unsubscribed.');
            isSubscribed = false;
        });
}
/////////////////////////////////////////////////////////////////////

jQuery(document).ready(function($){
    $("#main-menu li").each(function(){
        if ($(this).children()[0].href == window.location.href) {
            $(this).addClass("current");
        }
    });
    $(".extend-checkbox").click(function () {
        let checkBox = $(this).children("input[type=\"checkbox\"]");
        if (checkBox.length != 0) {
            checkBox.eq(0).click();
        }
    });
    $(".extend-checkbox input[type=\"checkbox\"]").click(function (e) {
        e.stopPropagation();
    });
});