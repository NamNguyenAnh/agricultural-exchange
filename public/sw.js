var cacheName = 'agex-v1.6.3';
var dataCacheName = 'data-agex-v1.6.1';
var domain = 'https://speakitout.net';
// var domain = 'http://localhost:8000';
var filesToCache = [
    '/',
    '/product-frame',
    '/no-connection',

    'favicon.ico',
    '/storage/images/agex-logo.png',
    '/storage/images/loading.gif',

    '/manifest.json',
    '/js/app.js',
    '/js/main.js',
    '/css/main.css',
    '/css/app.css',

    '/fontawesome/css/all.min.css',
    '/fontawesome/webfonts/fa-regular-400.woff2',
    '/fontawesome/webfonts/fa-solid-900.woff2',
];
var linkToCache = [
    '/api/products',
    '/api/history',
]

self.addEventListener('install', function(e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
    self.skipWaiting();
});

self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] Activate');
    e.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );
    return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
    console.log('[ServiceWorker] Fetch ', e.request.url);
    if (isCachedApiLink(e.request.url)) {
        e.respondWith(
            caches.open(dataCacheName).then(function (cache) {
                return fetch(e.request).then(function (response) {
                    if (response.status == 200) {
                        cache.put(e.request.url, response.clone());
                    }
                    return response;
                });
            })
        );
    } else {
        e.respondWith(
            caches.match(e.request).then(function (response) {
                if (e.request.url.indexOf(`${domain}/product`) > -1) {
                    return caches.match(`${domain}/product-frame`).catch(function (err) {
                        return caches.match(`${domain}/no-connection`);
                    });
                }
                return response || fetch(e.request).catch(err => {
                    return caches.match(`${domain}/no-connection`).then(response => {
                        return response || err;
                    });
                });
            })
        );
    }
});

function isCachedApiLink(url) {
    return linkToCache.reduce(function (total, current) {
        let result = url.indexOf(`${domain}${current}`) > -1 ? true : false;
        return total || result;
    }, false);
}

self.addEventListener('push', function(event) {
    console.log('[Service Worker] Push Received.');
    let data = JSON.parse(event.data.text());
    console.log(data);

    const options = {
        body: data.body,
        icon: data.icon,
        // badge: 'images/badge.png'
    };

    const notificationPromise = self.registration.showNotification(data.title, options);
    event.waitUntil(notificationPromise);
});

self.addEventListener('notificationclick', function(event) {
    console.log('[Service Worker] Notification click Received.');

    event.notification.close();

    event.waitUntil(
        event.waitUntil(clients.openWindow(`${domain}/profile`))
    );
});

self.addEventListener('updatefound', function (event) {
    console.log("Update caches");
    e.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
});