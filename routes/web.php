<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \Illuminate\Support\Facades\Route;

Route::view('/', 'home')->name('home');
Route::view('/introduce', 'introduce');
Route::view('/statistic', 'statistic');
Route::view('/profile', 'profile');
Route::view('/product-frame', 'product');
Route::view('/no-connection', 'no-connection');

Route::get('/product/{id}', 'ProductController@loadProductPage');
Route::get('/test', 'UserController@getWishHistory');

//Auth::routes();


Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');
Route::view('/registered', 'auth.registered');

Route::group(
    [
        'prefix' => 'admin',
    ],
    function () {

        Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('/login', 'Auth\LoginController@login');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

        Route::group(
            ['middleware' =>
                [
                    'admin:' . \Illuminate\Support\Facades\Config::get('capabilities.ACCESS_PANEL'),
                    'auth',
                ]
            ],
            function () {
                Route::get('/', 'AdminController');
                Route::group([
                    'middleware' => 'admin:' . \Config::get('capabilities.PRODUCT_MANAGER'),
                ], function () {
                    Route::get('/products', 'ProductController@adminPanel');
                    Route::post('/product/insert/', 'ProductController@insertProduct');
                    Route::get('/product/delete/{id}', 'ProductController@deleteProduct');
                    Route::get('/product/update/{id}', 'ProductController@updateProduct');
                });
                Route::group([
                    'middleware' => 'admin:' . \Config::get('capabilities.USER_MANAGER')
                ], function () {
                    Route::get('/users', 'UserController@adminPanel');
                    Route::post('/user/insert/', 'UserController@insertUser');
                    Route::get('/user/ban/{id}', 'UserController@banUser');
                    Route::get('/user/unban/{id}', 'UserController@unbanUser');
                });

                Route::group([
                    'prefix' => 'role'
                ], function () {
                    Route::get('/', 'RoleController@index');
                    Route::post('/insert/', 'RoleController@insertRole');
                    Route::get('/modify/{id}', 'RoleController@modifyRoleCapability');
                    Route::get('/delete/{id}', 'RoleController@deleteRole');
                    Route::get('/grant/', 'RoleController@grantUserRole');
                    Route::get('/revoke/{user_id}', 'RoleController@revokeUserRole');
                });
                Route::group([
                    'prefix' => 'setting',
                    'middleware' => 'admin:' . \Config::get('capabilities.SYSTEM_SETTING')
                ], function() {
                    Route::get('/', 'SettingController@index');
                    Route::post('/save', 'SettingController@save');
                });
            }
        );
    }
);
//Auth::routes(['verify' => true]);