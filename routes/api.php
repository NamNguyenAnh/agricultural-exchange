<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/history', 'UserController@getWishHistory');
    Route::get('/user', function(Request $request){
        return $request->user();
    });
    Route::post('/logout', 'TokenController@revokeToken');
    Route::post('/add-wish-list', 'WishItemController@addNewWishItem');

    Route::resource('/wish-item', 'WishItemController');
    Route::resource('/trade-log', 'TradeLogController');

    Route::post('/subscribe', 'UserController@subscribe');
    Route::post('/unsubscribe', 'UserController@unsubscribe');
});

Route::get('/products', 'ProductController@index');
Route::get('/product/{id}', 'ProductController@show');
Route::get('/wish-list', 'WishItemController@index');
Route::post('/login', 'TokenController@getPersonalToken');
Route::get('/product-list', 'StatisticController@getProductList');
Route::get('/statistic', 'StatisticController@getStatistic');
