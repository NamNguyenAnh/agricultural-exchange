<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('buy_wish');
            $table->unsignedInteger('sell_wish');
            $table->integer('price');
            $table->integer('quantity');
            $table->string('status', 20);
            $table->tinyInteger('buyer_rating', false, true)->nullable()->default(null);
            $table->tinyInteger('seller_rating', false, true)->nullable()->default(null);
            $table->timestamp('matched_at');
            $table->timestamp('updated_at')->nullable();

            $table->foreign('buy_wish')->references('id')->on('wish_items');
            $table->foreign('sell_wish')->references('id')->on('wish_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_logs');
    }
}
