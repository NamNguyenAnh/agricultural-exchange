<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             RoleSeed::class,
             UserSeed::class,
             ProductSeed::class,
             PriceLogSeed::class,
             WishItemSeed::class,
             TradeLogSeed::class,
             PassportSeed::class,
         ]);
    }
}
