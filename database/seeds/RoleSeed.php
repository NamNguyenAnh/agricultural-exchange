<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => '1',
            'name' => 'member',
            'capabilities' => json_encode([]),
        ]);
        DB::table('roles')->insert([
            'id' => '2',
            'name' => 'administrator',
            'capabilities' => json_encode([1, 2, 3, 4, 5]),
        ]);
    }
}
