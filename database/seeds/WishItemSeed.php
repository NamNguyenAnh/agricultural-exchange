<?php

use Illuminate\Database\Seeder;

class WishItemSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\WishItem::class, 150)->create();
    }
}
