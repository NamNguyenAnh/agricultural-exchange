<?php

use Illuminate\Database\Seeder;

class PriceLogSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 30; $i > 0; $i--)
        {
            for ($j = 1; $j <= 20; $j++)
            {
                $log = new App\PriceLog();
                $log->product_id = $j;
                $log->date = \Carbon\Carbon::now()->subDay($i + 1 )->toDateTimeString();
                $log->price = rand(290, 370) * 100;
                $log->quantity = rand(50, 256) * 100;
                $log->save();
            }
        }
    }
}
