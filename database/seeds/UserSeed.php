<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new App\User();
        $admin->name = 'Memphis';
        $admin->email = 'abc@123.com';
        $admin->password = bcrypt('123456');
        $admin->email_verified_at = \Carbon\Carbon::now()->toDateTimeString();
        $admin->role_id = 2;
        $admin->phone = '0123456789';
        $admin->save();

        $user = new App\User();
        $user->name = 'User 1';
        $user->email = 'namnguyen.cs13@gmail.com';
        $user->password = bcrypt('123456');
        $user->email_verified_at = \Carbon\Carbon::now()->toDateTimeString();
        $user->role_id = 1;
        $user->phone = '0123456788';
        $user->save();

        factory(App\User::class, 10)->create();
    }
}
