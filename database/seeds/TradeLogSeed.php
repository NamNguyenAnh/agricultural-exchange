<?php

use Illuminate\Database\Seeder;

class TradeLogSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\TradeLog::class, 100)->create();
    }
}
