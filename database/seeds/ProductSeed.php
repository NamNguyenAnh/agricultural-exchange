<?php

use Illuminate\Database\Seeder;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = json_decode(\Illuminate\Support\Facades\File::get("database/data/products.json"));
        foreach ($products as $product) {
            factory(App\Product::class)->create([
                'name' => $product->name,
                'description' => $product->description
            ]);
        }
    }
}
