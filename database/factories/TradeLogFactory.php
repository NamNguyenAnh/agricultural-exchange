<?php

use Faker\Generator as Faker;
use \Illuminate\Support\Facades\Config;
use \Illuminate\Support\Facades\DB;

$factory->define(App\TradeLog::class, function (Faker $faker) {
    $product_id = rand(1, 20);
    $quantity = rand(1, 6) * 100;
    $price = rand(300, 320) * 100;
    $time = \Carbon\Carbon::today()->subDay(random_int(5, 10));
    $buy = DB::table('wish_items')->insertGetId([
        'type' => Config::get('wishtype.BUY'),
        'user_id' => rand(1, 11),
        'product_id' => $product_id,
        'price' => $price,
        'quantity' => 0,
        'total' => $quantity,
        'status' => Config::get('wishstatus.COMPLETED'),
        'created_at' => $time->toDateTimeString(),
    ]);

    $sell = DB::table('wish_items')->insertGetId([
        'type' => Config::get('wishtype.SELL'),
        'user_id' => rand(1, 11),
        'product_id' => $product_id,
        'price' => $price,
        'quantity' => 0,
        'total' => $quantity,
        'status' => Config::get('wishstatus.COMPLETED'),
        'created_at' => $time->addDays(2)->toDateTimeString(),
    ]);

    return [
        'buy_wish' => $buy,
        'sell_wish' => $sell,
        'price' => $price,
        'quantity' => $quantity,
        'status' => Config::get('wishstatus.COMPLETED'),
        'buyer_rating' => random_int(3, 5),
        'seller_rating' => random_int(3, 5),
        'matched_at' => \Carbon\Carbon::today()->subDay(random_int(1, 5))->toDateTimeString(),
        'updated_at' => $time->addDays(2)->toDateTimeString()
    ];
});
