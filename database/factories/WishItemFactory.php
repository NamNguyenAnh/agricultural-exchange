<?php

use Faker\Generator as Faker;
use \Illuminate\Support\Facades\Config;

$factory->define(App\WishItem::class, function (Faker $faker) {
    $wish_type = Config::get('wishtype');
    $wish_type = $wish_type[array_rand($wish_type)];
    $price = 0;
    $quantity = rand(1, 6) * 100;
    if ($wish_type == Config::get('wishtype.BUY')) {
        $price = rand(280, 300) * 100;
    } else {
        $price = rand(301, 330) * 100;
    }
    return [
        'type' => $wish_type,
        'user_id' => rand(1, 11),
        'product_id' => rand(1, 20),
        'price' => $price,
        'quantity' => $quantity,
        'total' => $quantity,
        'status' => Config::get('wishstatus.PROCESSING'),
        'created_at' => \Carbon\Carbon::today()->subDay(random_int(0, 10))->toDateTimeString(),
    ];
});
