<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    $price = rand(290, 320) * 100;
    return [
        'name' => $faker->words(3, true),
        'unit' => 'kg',
        'description' => $faker->paragraphs(2, true),
        'old_price' => $price,
        'deviation' => rand(12, 56) * 100,
        'max_price' => $price + 3200,
        'min_price' => $price - 1400,
        'created_at' => $faker->dateTime,
    ];
});
